/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{html,ts}"
  ], // ** - all folders, * - all files, html, ts - extensions should be included
  theme: {
    extend: {},
  },
  plugins: [],
}
