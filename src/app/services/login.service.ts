import { Injectable } from '@angular/core';
import { map, Observable, of, switchMap } from 'rxjs';
import { User } from '../models/user.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const { apiUsers, apiKey } = environment;

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  // dependency injection, component are not injectable
  constructor(private readonly http: HttpClient) { // private does not give component to give acces to httpClient


  }

  // Models, HttpClient, Observables and RXJS operators
  public login(username: string): Observable<User> {
    return this.checkUsername(username)
      .pipe(
        switchMap((user: User | undefined) => {
          if (user === undefined) {
            return this.createUser(username);
          }
          return of(user);
        })
      )
  }
  // Login

  // Check if user exixsts
  private checkUsername(username: string): Observable<User | undefined> {
    return this.http.get<User[]>(`${apiUsers}?username=${username}`)
      .pipe(
        //RxJS Operators
        map((response: User[]) => response.pop() // pop takes the last item in the array and return it back to the app and returned value will a single user
        )
      )
  }

  // create a user
  private createUser(username: string): Observable<User> {
    const user = {
      username,
      favourites: []
    };
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-api-key": apiKey
    });
    return this.http.post<User>(apiUsers, user, {
      headers
    })
    // headers -> APIkey
    // POST - Create items on server
  }

  // If User exixsts from Login || Created User -> Store user
}
