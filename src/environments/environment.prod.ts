export const environment = {
    production: true,
    apiUsers: "https://zaru-experis-api.herokuapp.com/users",
    apiGuitars: "https://zaru-experis-api.herokuapp.com/guitars"
};